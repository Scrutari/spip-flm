# Films en Luttes et en Mouvements, Plateforme internet du cinéma militant

[Films en Luttes et en Mouvements](http://www.films-luttes-mouvements.net)  est un catalogue collectif autour des mouvements sociaux initié par l’association Autour du 1er mai et le Cedidelp. Il utilise Scrutari pour centraliser les informations collectées auprès des bases de données des membres.

Le site lui-même est sous Spip. Ce dépôt contient les squelettes utilisés ainsi que les ressources statiques (Javascript et CSS).

Scrutari est utilisé de trois manières :

## Affichage en HTML

C'est le squelette squelettes/scrutari.html ainsi que les fonctions PHP incluses dans squelettes/scrutari/ qui assurent le traitement (récupération des termes recherchés, interrogation du moteur, transformation des résultats).

## Client ScrutariJs

Le squelette  squelettes/scrutarijs.html est un exemple de l'intégration dans Spip du client en pur Javascript. Le fichier static/js/scrutari-flm.js sert à adapter ScrutariJs (actuellement uniquement le remplace du titre)


## Dans les sélections thématiques

Les sélections thématiques proposent des listes de films sélectionnés « manuellement » issus des différents catalogues des participants, voir par exemple [Alimentation et consommation](http://www.films-luttes-mouvements.net/article5.html).

Le modèle squelettes/modeles/liste.html est utilisé au sein du texte d'un article pour indiquer l'insertion d'une liste de films. Il demande simplement d'indiquer la base d'origine et l'identifiant du film comme le montre l'exemple de la sélection sur Alimentation et consommation :

    <liste|fiches=cedidelp/51437,alimenterre/16,premiermai/3355,autresbresils/332,autresbresils/61,alimenterre/161,cedidelp/55712,alimenterre/57,alimenterre/2635,alimenterre/6408,cedidelp/50582,premiermai/3337,alimenterre/2632,alimenterre/773,autresbresils/123>
 
Le modèle se charge ensuite d'interroger Scrutari pour récupérer le titre, les réalisateurs, une description courte.


