<?php

define("FLM", "FLM");
define("FLM_SCRUTARIURL", 'http://sct1.scrutari.net/sct/flm/');

/*************************************************************************
* Balises  #FLM_
**************************************************************************/
include('scrutari/fonctions.php');

/**
* Balise retournant le chemin des fichiers statiques de ressources
*/
function balise_FLM_RSCPATH($p) {
    $p->code = "flm_balise_RscPath()";
    return $p;
}


function balise_FLM_INIT_SCRUTARI($p) {
    $q = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
        $q = $v;
    }
    $p->code = "flm_balise_initScrutari($q)";
    return $p;
}

function balise_FLM_LIEN($p) {
    $base = "''";
    $id = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
        $base = $v;
        if (($v = interprete_argument_balise(2,$p))!==NULL) {
            $id = $v;
        }
    }
    $p->code = "flm_balise_getLien($base, $id)";
    return $p;
}

function balise_FLM_LIEN_TITLE($p) {
    $base = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
        $base = $v;
    }
    $p->code = "flm_balise_getLienTitle($base)";
    return $p;
}

function balise_FLM_SCRUTARI_LISTE($p) {
    $p->code = "flm_balise_scrutari_getListe()";
    return $p;
}

function balise_FLM_SCRUTARI_BASES($p) {
    $p->code = "flm_balise_scrutari_getBases()";
    return $p;
}

function balise_FLM_SCRUTARI_FICHES($p) {
    $p->code = "flm_balise_scrutari_getFiches()";
    return $p;
}

function balise_FLM_SCRUTARI_FICHECOUNT($p) {
    $p->code = "flm_balise_scrutari_getFicheCount()";
    return $p;
}

function balise_FLM_LISTE_FICHES($p) {
    $base = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
        $base = $v;
    }
    $p->code = "flm_balise_liste_getFiches($v)";
    return $p;
}

function balise_FLM_BASE_SOUSTITRE($p) {
    $base = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
        $base = $v;
    }
    $p->code = "flm_balise_base_getSoustitre($v)";
    return $p;
}

/*************************************************************************
* filtres |flm_
**************************************************************************/

function filtre_flm_resultats($texte) {
    if ($texte == 1) {
        return "un résultat";
    } else {
        return $texte." résultats";
    }
}

function filtre_flm_titre_classe($texte) {
    if (strpos($texte, 'FLM') === 0) {
        return 'TitreFLM';
    } else {
        return '';
    }
}

function filtre_flm_titre_span($texte) {
    if (strpos($texte, 'FLM') === 0) {
        return '<span class="TitreFLM_FLM">FLM</span>'.substr($texte, 3);
    } else {
        return $texte;
        }
}



/*************************************************************************
*Fonctions de résolution des balises
**************************************************************************/

function flm_balise_getLien($base, $id) {
    if ($base == 'premiermai') {
        return "http://www.autourdu1ermai.fr/bdf_fiche-film-$id.html";
    }
    if ($base == 'cedidelp') {
        return "http://www.ritimo.fr/opac_css/index.php?lvl=notice_display&id=$id";
    }
    if ($base == 'alimenterre') {
        return "http://www.alimenterre.org/film/$id";
    }
    if ($base == 'socioeco') {
        return "http://www.socioeco.org/bdf_fiche-video-".$id."_fr.html";
    }
    if ($base == 'ripess') {
        return "http://www.ripess.org/".$id."/";
    }
    if ($base == 'histoireimmigration') {
        return "http://catalogue.histoire-immigration.fr/cgi-bin/koha/opac-detail.pl?biblionumber=".$id;
    }
    if ($base == 'autresbresils') {
        return "http://www.autresbresils.net/spip.php?page=fiche-film&idcorpus=".$id;
    }
    if ($base == 'bretagneetdiversite') {
        return "http://bed.bzh/fr/films/".$id."/";
    }
    return '';
}

function flm_balise_RscPath() {
    return "static/";
}

function flm_balise_getLienTitle($base) {
    if ($base == 'premiermai') {
        return "Fiche du film sur le site Autour du 1er mai";
    }
    if ($base == 'cedidelp') {
        return "Notice du catalogue Cedidelp";
    }
    if ($base == 'alimenterre') {
        return "Fiche du film sur le site Alimenterre";
    }
    if ($base == 'socioeco') {
        return "Fiche de la vidéo sur le site socioeco.org";
    }
    if ($base == 'ripess') {
        return "Vidéo en ligne sur le site Ripess";
    }
    if ($base == 'histoireimmigration') {
        return "Notice du musée de l’histoire de l’immigration";
    }
    if ($base == 'autresbresils') {
        return "Fiche de la vidéothèque d'Autres Brésils";
    }
    if ($base == 'bretagneetdiversite') {
        return "Fiche du film sur le site Bretagne et Diversité";
    }
    return '';
}

function flm_balise_initScrutari($q) {
    flm_getBases();
    if ($q) {
        $sct_log = 1;
        $sct_query = 'type=q-fiche&fichefields=codebase,codecorpus,mtitre,msoustitre,mcomplements,annee,mattrs_primary,href&motclefields=mlibelles&langui=fr&intitules=complement&q='.urlencode($q).'&origin='.$sct_site.'&log='.$sct_log;
        $sct_ficheSearchResult = Sct_initFicheSearchResult(FLM_SCRUTARIURL, $sct_query);
        $GLOBALS[FLM]['ficheSearchResult'] = $sct_ficheSearchResult;
    }
}

/*
* Retourne un tableau des bases où chaque base est réprésentée par un tableau associatifs
* les clés sont celles de l'objet base renvoyé en Json par type=base plus la clé total
* qui indique le nombre de résultats
*/
function flm_balise_scrutari_getBases() {
    $avecResultat = false;
    if (array_key_exists('ficheSearchResult', $GLOBALS[FLM])) {
        if ($GLOBALS[FLM]['ficheSearchResult']['ficheCount'] > 0) {
            $avecResultat = true;
        }
    }
    if (!$avecResultat) {
        return array();
    }
    return Sct_selectBases($GLOBALS[FLM]['bases'], $GLOBALS[FLM]['ficheSearchResult']);
}

function flm_balise_scrutari_getFiches() {
    $avecResultat = false;
    if (array_key_exists('ficheSearchResult', $GLOBALS[FLM])) {
        if ($GLOBALS[FLM]['ficheSearchResult']['ficheCount'] > 0) {
            $avecResultat = true;
        }
    }
    if (!$avecResultat) {
        return array();
    }
    return Sct_getFicheArray(flm_getBases(), $GLOBALS[FLM]['ficheSearchResult']);
}

function flm_balise_scrutari_getListe() {
    if (array_key_exists('ficheSearchResult', $GLOBALS[FLM])) {
        return Sct_printResult( $GLOBALS[FLM]['ficheSearchResult']);
    }
    return "";
}

function flm_balise_scrutari_getFicheCount() {
    if (array_key_exists('ficheSearchResult', $GLOBALS[FLM])) {
        return $GLOBALS[FLM]['ficheSearchResult']['ficheCount'];
    }
    return 0;
}

function flm_balise_liste_getFiches($fiches) {
    $ficheArray = explode(',', $fiches);
    $count = count($ficheArray);
    $codeficheArray = array();
    for($i = 0; $i < $count; $i++) {
        $fiche = $ficheArray[$i];
        $idx = strpos($fiche, '/');
        if ($idx > 0) {
            $codecorpus = flm_getCodecorpus(substr($fiche, 0, $idx));
            if ($codecorpus > 0) {
                $codeficheArray[] = $codecorpus.'/'.substr($fiche, $idx + 1);
            }
        }
    }
    if (count($codeficheArray) == 0) {
        return array();
    }
    $query = 'type=fiche&fichefields=codebase,codecorpus,titre,soustitre,annee,href&langui=fr&fichelist='.implode(',', $codeficheArray);
    return Sct_getFicheArrayFromFicheData(FLM_SCRUTARIURL, $query, flm_getBases());
}

function flm_balise_base_getSoustitre($code) {
    if (!is_int($code)) {
        $code = flm_getCodebase($code);
    }
    switch($code) {
        case 1:
            return "base des films qui interrogent la société";
        case 3741 :
            return "médiathèque des mouvements sociaux";
        case 9568:
            return "festival de films sur les enjeux agricoles et alimentaires";
        case 10782:
            return "site ressources de l’économie sociale et solidaire";
        case 10993:
            return "réseau intercontinental de promotion de l'économie sociale et solidaire"; 
        case 11570:
            return "musée de l’histoire de l’immigration";
        case 12742:
            return "décryptage de la société brésilienne pour un public francophone";
        case 14822:
            return "films emblématiques de la diversité culturelle dans le monde";
        default :
            return "";
    }
}



/*************************************************************************
*Fonctions de correspondance
**************************************************************************/

/*
* Retourne les codes dans le moteur Scrutari correspondant aux noms de base
*/
function flm_getCodebase($base) {
    if ($base == 'premiermai') {
        return 1;
    }
    if ($base == 'cedidelp') {
        return 3741;
    }
    if ($base == 'alimenterre') {
        return 9568;
    }
    if ($base == 'socioeco') {
        return 10782;
    }
    if ($base == 'ripess') {
        return 10993;
    }
    if ($base == 'histoireimmigration') {
        return 11570;
    }
    if ($base == 'autresbresils') {
        return 12742;
    }
    if ($base == 'bretagneetdiversite') {
        return 14822;
    }
    return 0;
}

/*
* Retourne les codes de corpus dans le moteur Scrutari correspondant aux noms de base
*/
function flm_getCodecorpus($base) {
    if ($base == 'premiermai') {
        return 2;
    }
    if ($base == 'cedidelp') {
        return 3742;
    }
    if ($base == 'alimenterre') {
        return 9569;
    }
    if ($base == 'socioeco') {
        return 10783;
    }
    if ($base == 'ripess') {
        return 10994;
    }
    if ($base == 'histoireimmigration') {
        return 11571;
    }
    if ($base == 'autresbresils') {
        return 12743;
    }
    if ($base == 'bretagneetdiversite') {
        return 14823;
    }
    return 0;
}

/****************************************
* Utilitaires
*/

function flm_getBases() {
    if (!array_key_exists(FLM, $GLOBALS)) {
        $GLOBALS[FLM] = array();
    }
    if (array_key_exists('bases', $GLOBALS[FLM])) {
        return $GLOBALS[FLM]['bases'];
    }
    $bases = Sct_initBases(FLM_SCRUTARIURL);
    $GLOBALS[FLM]['bases'] = $bases;
    return $bases;
}
