<?php

/*
* Récupération de la réponse JSON  sous forme de chaine et transformation sous la forme d'objet PHP.
* $obj est en fait un tableau, on pourrait récupérer $obj sous forme d'objet avec json_decode(false)
*/
function Sct_jsonQuery($url, $query) {
    $jsonString  = file_get_contents($url.'JSON?'.$query);
    $obj = json_decode($jsonString, true);
    return $obj;
}

function Sct_initBases($url) {
    $result = array();
    $query = 'type=base&langui=fr';
    $obj = Sct_jsonQuery($url, $query);
    $baseData = $obj["baseData"];
    $count = $baseData["baseCount"];
    $array = $baseData["baseArray"];
    for($i = 0; $i < $count; $i++) {
        $base = $array[$i];
        $key = 'codebase_'.$base['codebase'];
        $result[$key] = $base;
    }
    return $result;
}

function Sct_getFicheArrayFromFicheData($url, $query, $bases) {
    $obj = Sct_jsonQuery($url, $query);
    $ficheData = $obj["ficheData"];
    if ($ficheData["ficheCount"] == 0) {
        return array();
    }
    $ficheArray = $ficheData["ficheArray"];
    $count = count($ficheArray);
    for($i = 0; $i < $count; $i++) {
        $fiche = $ficheArray[$i]; 
        $base = $bases['codebase_'.$fiche['codebase']];
        $fiche['icon'] = $base['baseicon'];
        $ficheArray[$i] = $fiche;
    }
    return $ficheArray;
}

/*
* Fonction d'initialisation qui effectue la requête auprès du serveur Scrutari, récupère le résultat
* au format JSON et le traduit en HTML.
* La valeur de retour est une chaine.
* - $url : l'url du serveur Scrutari avec la barre oblique de fin (exemple : http://sct1.scrutari.net/sct/coredem/)
* - $query : la chaine avec les paramètres de recherche (sera concaténé avec $url sous la forme $url.'JSON?'.$query pour construire
* l'interrogation auprès du serveur Scrutari), $query doit comprendre le paramètre type=q-fiche et doit également définir la langue
* de l'interface avec le paramètre langui
*/

function Sct_initFicheSearchResult($url, $query) {
    $obj = Sct_jsonQuery($url, $query);
    if (array_key_exists('ficheSearchResult', $obj)) {
        return $obj['ficheSearchResult'];
    } else {
        return null;
    }
}

function Sct_selectBases($bases, $ficheSearchResult) {
    $usedBaseMap = array();
    $ficheCount = $ficheSearchResult['ficheCount'];
    $ficheGroupArray = $ficheSearchResult['ficheGroupArray'];
    $groupCount = count($ficheGroupArray);
    for($i = 0; $i < $groupCount; $i++) {
        $ficheGroup = $ficheGroupArray[$i];
        $ficheArray = $ficheGroup['ficheArray'];
        $count = count($ficheArray);
        for($j = 0; $j < $count; $j++) {
            $fiche = $ficheArray[$j];
            $codebase = "codebase_".$fiche['codebase'];
            if (!array_key_exists($codebase, $usedBaseMap)) {
                $usedBaseMap[$codebase] = 1;
            } else {
                $usedBaseMap[$codebase]++;
            }
        }
    }
    arsort($usedBaseMap);
    $resultArray = array();
    foreach($usedBaseMap as $key => $value) {
        $base = $bases[$key];
        $base["total"] = $value;
        $resultArray[] = $base;
    }
    return $resultArray;
}

function Sct_getFicheArray($bases, $ficheSearchResult) {
    /*
    * Création d'une instance de la classe Sct_FicheSearchResultWrapper qui permettre un certain nombre de traitement
    * sur le résultat. 'SCT_Marque' est le nom de la classe appliquée aux balises <span> qui servent à surligner les termes
    * de la recherche
    */
    $sctFicheSearchResultWrapper = new Sct_FicheSearchResultWrapper($ficheSearchResult, 'SCT_Marque');
    $result = array();
    $ficheGroupArray = $ficheSearchResult['ficheGroupArray'];
    $groupCount = count($ficheGroupArray);
    for($i = 0; $i < $groupCount; $i++) {
        $ficheGroup = $ficheGroupArray[$i];
        $ficheArray = $ficheGroup['ficheArray'];
        $count = count($ficheArray);
        for($j = 0; $j < $count; $j++) {
            $fiche = $ficheArray[$j];
            $codecorpus = $fiche['codecorpus'];
            $base = $bases['codebase_'.$fiche['codebase']];
            $fiche['icon'] = $base['baseicon'];
            $fiche['titre'] = $sctFicheSearchResultWrapper->concatWithSpan($fiche['mtitre']);
            if (array_key_exists('msoustitre', $fiche)) {
                $fiche['soustitre'] = $sctFicheSearchResultWrapper->concatWithSpan($fiche['msoustitre']);
            }
            if (array_key_exists('mattrMap', $fiche)) {
                $mattrMap = $fiche['mattrMap'];
                if (array_key_exists('sct:authors', $mattrMap)) {
                    $realisateursArray = $mattrMap['sct:authors'];
                    $realisateursCount = count($realisateursArray);
                    $realisateurs = "";
                    for($k = 0; $k <  $realisateursCount; $k++) {
                        if ($k > 0) {
                            $realisateurs .= ', ';
                        }
                        $realisateurs .= $sctFicheSearchResultWrapper->concatWithSpan($realisateursArray[$k]);
                    }
                    $fiche['realisateurs'] = $realisateurs;
                }
            }
            if (array_key_exists('mcomplementArray', $fiche)) {
                $compArray = $fiche['mcomplementArray'];
                $compCount = count($compCount);
                $compResult = array();
                for($k = 0; $k < $compCount; $k++) {
                    $comp = $compArray[$k];
                    $compVal = $sctFicheSearchResultWrapper->concatWithSpan($comp['mcomp']);
                    $intitule = $sctFicheSearchResultWrapper->getComplementIntitule($codecorpus, $comp['num']);
                    $compResult[] = array("intitule" => $intitule, "valeur" => $compVal);
                }
                $fiche['complements'] = $compResult;
            }
            if (array_key_exists('codemotcleArray', $fiche)) {
                $codemotcleArray = $fiche['codemotcleArray'];
                $motcleCount = count($codemotcleArray);
                $motscles = array();
                if ($motcleCount == 1) {
                    $motscles["intitule"] = "Mot-clé";
                } else {
                    $motscles["intitule"] = "Mots-clés";
                }
                $resultString = "";
                for($k = 0; $k < $motcleCount; $k++) {
                    if ($k > 0) {
                        $resultString .=  ', ';
                    }
                    $codemotcle = $codemotcleArray[$k];
                    $resultString .= $sctFicheSearchResultWrapper->getMotcleString($codemotcle);
                }
                $motscles["valeur"] = $resultString;
                $fiche['motscles'] = $motscles;
            }
            $result[] = $fiche;
        }
    }
    return $result;
}



 
 
/**
* Objet encapsulant le résultat d'une recherche et proposant
* des fonctions utilitaires.
*/
class Sct_FicheSearchResultWrapper {
 
    var $corpusIntituleArray;
    var $spanClass; //contenu de l'attribut class des balises span
    var $motcleStringMap; //tableau associatif avec comme clé le code d'un mot-clé et comme valeur le libellé du mot-clé
 
    function __construct($ficheSearchResult, $spanClass) {
        $this->corpusIntituleArray = $ficheSearchResult['corpusIntituleArray'];
        $this->spanClass = $spanClass;
        $this->motcleStringMap = $this->initMotcleStringMap($ficheSearchResult);
    }
 
    /**
    * Retourne l'intitulé du complément de numéro $compNum pour
    * le corpus de code $codecorpus
    */
    function getComplementIntitule($codecorpus, $compNum) {
        $intituleCount = count($this->corpusIntituleArray);
        for($i = 0; $i < $intituleCount; $i++) {
            $corpusIntitule = $this->corpusIntituleArray[$i];
            if ($corpusIntitule['codecorpus'] == $codecorpus) {
                return $corpusIntitule['complement_'.$compNum];
            }
        }
        return "?";
    }
 
    /**
    * $markedStringArray est un tableau qui contient soit des chaines soit des tableaux associatifs
    * comprenant une clé 's' indiquant les chaines marquées. Ces dernières sont entourées d'une balise
    * span dont la classe est $spanClass
    */
    function concatWithSpan($markedStringArray) {
        $result = "";
        $count = count($markedStringArray);
        for($i = 0; $i < $count; $i++) {
            $obj = $markedStringArray[$i];
            if (is_array($obj)) {
                $result .= '<span class="'.$this->spanClass.'">';
                $result .= $obj['s'];
                $result .= '</span>';
            } else {
                $result .= $obj;
            }
        }
        return $result;
    }
 
    /**
    * Retourne le libellé du mot-clé de code $codemotcle
    */
    function getMotcleString($codemotcle) {
        return $this->motcleStringMap[$codemotcle];
    }
 
    /**
    * Initialisation du tableau associatif des codes de mots-clés et de lerus libellés
    */
    private function initMotcleStringMap($ficheSearchResult) {
        $motcleStringMap = array();
        if (!array_key_exists('motcleArray', $ficheSearchResult)) {
            return $motcleStringMap;
        }
        $motcleArray = $ficheSearchResult['motcleArray'];
        $count = count($motcleArray);
        for($i = 0; $i < $count; $i++) {
            $motcle = $motcleArray[$i];
            $codemotcle = $motcle['codemotcle'];
            $mlibelleArray = $motcle['mlibelleArray'];
            $lib = "";
            $libCount = count($mlibelleArray);
            for($j = 0; $j < $libCount; $j++) {
                if ($j > 0) {
                    $lib .= "/";
                }
                $mlib =$mlibelleArray[$j];
                $lib .= $this->concatWithSpan($mlib['mlib']);
            }
            $motcleStringMap[$codemotcle] = $lib;
        }
        return $motcleStringMap;
    }
 
}
