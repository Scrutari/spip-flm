$(function() {
    var inputEl = $("#ID_INPUT_SCRUTARI");
    inputEl.val("Recherche (titre, réalisateur, mot-clé…)");
    inputEl.addClass("jq_IntroRecherche");
    inputEl.focus(function () {
        var el = $(this);
        if (el.hasClass("jq_IntroRecherche")) {
            el.val("");
            el.removeClass("jq_IntroRecherche");
        }
    });
});