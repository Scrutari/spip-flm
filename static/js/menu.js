
function updateFleche (ouvert) {
    if (ouvert) {
         $("#thematiquesFleche").html("▼");
    } else {
        $("#thematiquesFleche").html("▶");
    }
}


$(function() {
    $("#thematiquesTitre span.Libelle").append(" <span id='thematiquesFleche'></span>");
    updateFleche(!$("#thematiquesMenu").hasClass("ListeCachee"));
    $("#thematiquesTitre").click(function () {
        var ulEl = $("#thematiquesMenu");
        if (ulEl.hasClass("ListeCachee")) {
            ulEl.removeClass("ListeCachee");
            updateFleche(true);
        } else {
            ulEl.addClass("ListeCachee");
            updateFleche(false);
        }
    });
});