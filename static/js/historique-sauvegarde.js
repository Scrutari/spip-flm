var Scrutari_Historique = 10;

storeRecherche();

function rotation(numero) {
    var item = localStorage.getItem("recherche_" + numero);
    if (item) {
        numero = numero + 1;
        localStorage.setItem("recherche_" + numero, item);
    }
}

function storeRecherche () {
    if (!localStorage) {
        return;
    }
    if (Scrutari_ficheCount == 0) {
        return;
    }
    var seuil = Scrutari_Historique - 1;
    var stop = false;
    for(var i= 1; i <= Scrutari_Historique; i++) {
        var item = localStorage.getItem("recherche_" + i);
        stop = false
        if (item) {
            var obj = $.parseJSON(item);
            if (obj.q == Scrutari_q) {
                stop = true;
            }
        } else {
            stop = true;
        }
        if (stop) {
            seuil = i -1;
            break;
        }
    }
    for(var i = seuil; i > 0; i--) {
        rotation(i);
    }
    var obj = {count: Scrutari_ficheCount, q: Scrutari_q};
    localStorage.setItem("recherche_1", $.toJSON(obj));
}