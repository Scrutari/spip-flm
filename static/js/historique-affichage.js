function getSauvegarde(numero) {
    var item = localStorage.getItem("recherche_" + numero);
    if (!item) {
        return false;
    }
    return $.parseJSON(item);
}

$(function() {
    if (!localStorage) {
        return;
    }
    var withQ = false;
    var texte = "";
    for (var i = 1; i < 11; i++) {
        var obj = getSauvegarde(i);
        if (obj) {
            texte += "<li><a href='scrutari?q=" + encodeURI(obj.q) + "'";
            if (!withQ) {
                 texte += " class='DerniereRecherche'";
                withQ = true;
            }
            texte += ">" + obj.q + " <span>(" + obj.count + ")</span></a></li>";
            
        }
    }
    if (withQ) {
        $("#globalMenu").append("<li class='Historique hidden-xs hidden-sm'><p>Historique de votre recherche</p><ul class='Recherches'>" + texte + "</ul></li>");
    }
});